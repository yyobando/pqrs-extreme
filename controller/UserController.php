<?php 

// require_once("../model/Responses.php");
// require_once("../dataBase/Connector.php");
// require_once("../model/User.php");
require_once("../model/User.php");
require_once("../databd/connection.php");
require_once("../model/Token.php");

class UserController{

    function __construct()
    {
        $data = User::getListInObjects(null);
        $this->users = $data;
    }
}
$user = new User(null,null);
$token = new Token();

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $params = file_get_contents("php://input");
    $params = json_decode($params);
   
    if (isset($params->action)) {
        if ($params->action == 'login') {

           $user = $user->userLogin($params);
           if ($user['error'] == null) {
                $response['status'] = 'success';
                $response['data'] = $user;
                header("HTTP/1.1  200 OK");
                echo json_encode($response,JSON_UNESCAPED_UNICODE);
                exit;
           }else {
            $response['status'] = 'error';
            $response['message'] = ($user['error']);
            header("HTTP/1.1  200 OK");
            echo json_encode($response, JSON_UNESCAPED_UNICODE);
            exit;
           }

        }elseif($params->action == 'register'){
            $headers = apache_request_headers();
            $token->validateToken($headers['Authorization']);
            $result = $user->registerUser($params);
            if ($result['error'] == null) {
                $response['status'] = 'success';
                $response['message'] = $result['success'];
                header("HTTP/1.1  200 OK");
                echo json_encode($response,JSON_UNESCAPED_UNICODE);
                exit;
            }else{
                $response['status'] = 'error';
                $response['message'] = $result['error'];
                header("HTTP/1.1  200 OK");
                echo json_encode($response, JSON_UNESCAPED_UNICODE);
                exit; 
            }
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Accion requerida';
            header("HTTP/1.1  200 OK");
            echo json_encode($response, JSON_UNESCAPED_UNICODE);
            exit;
        }
    }else{
        $response['status'] = 'error';
        $response['message'] = 'Accion requerida';
        header("HTTP/1.1  200 OK");
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        exit;
    }
}elseif($_SERVER['REQUEST_METHOD'] == "GET"){
  
//    $headers = apache_request_headers();
//    $token->validateToken($headers['Authorization']);
   
   $users = $user->getUser(null);
   if($users){
    $response['data'] = $users;
    header("HTTP/1.1  200 OK");
   }else {
    $response['status'] = 'error';
    $response['message'] = 'No existen datos';
    header("HTTP/1.1  200 OK");
    exit;
   }

   echo json_encode($response, JSON_UNESCAPED_UNICODE);
}