<?php
require_once("../model/Pqrs.php");
require_once("../model/User.php");
require_once("../databd/connection.php");
require_once("../model/Token.php");

$user = new User(null,null);
$token = new Token();
$connection = new Connection();
$pqrs = new Pqrs();

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $headers = apache_request_headers();
    $token->validateToken($headers['Authorization']);

    $params = file_get_contents("php://input");
    $params = json_decode($params);
    if (empty($params->id)) {
        $result = $pqrs->savePqrs($params);
        $response['status'] = ($result['error'] == null ) ? 'success':'error';
        $response['message'] = ($result['error'] == null ) ? $result['success'] : $result['error'] ;
        
    }else{
        
        $result = $pqrs->editPqrs($params);
        $response['status'] = ($result['error'] == null ) ? 'success':'error';
        $response['message'] = ($result['error'] == null ) ? $result['success'] : $result['error'] ;
        
    }
    
    header("HTTP/1.1  200 OK");
    echo json_encode($response, JSON_UNESCAPED_UNICODE);

}elseif($_SERVER['REQUEST_METHOD'] == "GET"){

    $headers = apache_request_headers();
    $token->validateToken($headers['Authorization']);

    $result = $pqrs->getPqrsByUser($headers['Authorization']);
    if ($result['error'] == null) {
        $response['data'] = $result['data'];
    }else {
        $response['message'] = $result['error'];
    }
    header("HTTP/1.1  200 OK");
    echo json_encode($response, JSON_UNESCAPED_UNICODE);
}elseif($_SERVER['REQUEST_METHOD'] == "DELETE") {
    $headers = apache_request_headers();
    $token->validateToken($headers['Authorization']);
    if ($pqrs->deletePqrs($_GET['pqrs_id'])) {
        $response['status'] = 'success';
        $response['message'] = 'eliminado con exito';
    }else {
        $response['status'] = 'error';
        $response['message'] = 'ha ocurrido un error al eliminar';
    }
    header("HTTP/1.1  200 OK");
    echo json_encode($response, JSON_UNESCAPED_UNICODE);
}else {
    $response['message'] = 'metodo no soportado';
    header("HTTP/1.1  200 OK");
    echo json_encode($response, JSON_UNESCAPED_UNICODE);
}