<?php
class Connection {
    public $mysqli;

    public function __construct() {
  
      $DATABASE = 'api_pqrs';
      $MYSQL_USER = 'user_extreme';
      $MYSQL_PASSWORD = 'GbUG*u2ccK-n';
  
      // $DATABASE = 'api_amazongear_develop';
      // $MYSQL_USER = 'saving_155216';
      // $MYSQL_PASSWORD = 'GbUG*u2ccK-n';
  
      if (isset($_SERVER['SERVER_SOFTWARE']) && strpos($_SERVER['SERVER_SOFTWARE'],'Google App Engine') !== false) {
        $INSTANCE_CONNECTION_NAME = 'saving-the-amazon-155216:us-central1:mysql-saving-the-amazon';
        $MYSQL_DNS = "mysql:unix_socket=/cloudsql/$INSTANCE_CONNECTION_NAME;dbname=$DATABASE;charset=UTF8";
      } else {
        $MYSQL_DNS = 'mysql:host=localhost;dbname='.$DATABASE.';charset=UTF8';
      }
  
      try {
        $this->mysqli = new PDO($MYSQL_DNS, $MYSQL_USER, $MYSQL_PASSWORD);
      } catch (PDOException $e) {
        echo 'Falló la conexión: ' . $e->getMessage();
        exit;
      }
    }

    public function execute($query)
    {
        if (!empty($query)) {
            $sql = $this->mysqli->prepare($query);
            $sql->execute();
            $access = $sql->fetchAll(PDO::FETCH_ASSOC);

            if (count($access)>0) {
                $data = $access;
            } else {
                // $data = array('error'=>'No se encontraron datos');
                return false;
            }
            return $data;
        }else {
          //  return  array('error'=>'Ingresa una cadena valida');
            return false;
        }
    }

    public function save($data, $table)
    {
        $fields = $values = $fields_values = NULL;
        foreach ($data as $key => $value) {
            $fields .= $key.',';
            $values .= "'$value',";
        }

        $add_sql = "INSERT INTO $table (".trim($fields,',').") VALUES (".trim($values,',').")";

        if (!$this->mysqli->query($add_sql)) {
            $error = $this->mysqli->errorInfo();
            return false;
        } else {
            $id = $this->mysqli->lastInsertId();
            return array('id' => $id );
        }

    }

    public function update($data, $table, $filter)
    {
      $fields = $values = $fields_values = NULL;
      foreach ($data as $key => $value) {
          $fields .= $key."='$value',";
      }
      
      $update_sql = "UPDATE $table SET ".trim($fields,',')." WHERE $filter";

      if (!$this->mysqli->query($update_sql)) {
        $error = $this->mysqli->errorInfo();
        return false;
      } else {
        return true;
      }

    }

    public function delete($table, $id)
    {
      $query_sql = "DELETE FROM $table WHERE id ='$id'";
      if (!$this->mysqli->query($query_sql)) {
        return false;
      } else {
        return true;
      }
    }
}