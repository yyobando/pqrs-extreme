

$(function (e) {
   if (localStorage['token'] == null) {
    window.location="login-form.php";
   } 

   setMenu();
   getUser();

   $('#pqr-link').click(function () {
     getFormPqr();
     
   });

  

});

function setMenu() {
    html = '';
    if (localStorage['rol_name']== 'GENERAL_ADMINISTRATOR') {
        
        html+='<a class="collapse-item" href="main.html">Usuarios</a>';
        html+='<a class="collapse-item" href="#" id="pqr-link">PQRS</a>';
    }else if(localStorage['rol_name']== 'GENERAL_USER'){
        html+='<a class="collapse-item" href="#"  id="pqr-link" >PQRS</a>';
        console.log('entra');
        getFormPqr();
    }else{
       
        window.location="login-form.php";
    }
    $('#menu').html(html);
}
var table_user = null;
function getUser(){
    if (table_user != null) {
        table_user.clear();
        table_user.destroy();
        table_user = null;
    }
    table_user = $('#userTable').DataTable({
                "ajax": 'controller/UserController.php',
                "scrollX": true,
                "columns": [
                    {title: 'Id', data: 'id'},
                    {title: 'Usuario', data: 'user_name'},
                    {title: 'Rol', data: 'rol_name'},
                    {title: 'Estado', data: 'status_name'},
                    
                ]    

            });
}

$('form#form-user').submit(function (e) {
    $('#alert-register-user').hide();
   e.preventDefault();
   var jsonData = $(this).serializeArray().reduce(function(a, z) { a[z.name] = z.value; return a; }, {});
   $.ajax({
         url : 'controller/UserController.php',
         data : JSON.stringify(jsonData),
         type : 'POST',
         headers: {'Authorization':localStorage['token']},
         dataType : 'json',
         success : function(json) {
             if(json.status == "success"){
                getUser();
             }else{
                 $("#alert-register-user").show();
                 $('#alert-register-user').text(json.message);
             }
             
         },
         error : function(xhr, status) {
             $("#alert-register-user").show();
             $('#alert-register-user').text('error desconocido');
         },

     });
});


$('#pqr-link').click(function () {
    getFormPqr();
});

function  getFormPqr() {
    $.ajax({
        url : 'pqrs.php?role='+localStorage['rol_name'],
        success : function(html) {
            $('#content').html(html);
            getPqrs();
        },
        error : function(xhr, status) {
            $("#alert-register-user").show();
            $('#alert-register-user').text('error desconocido');
        },

    });
}



var table_prs = null;
function getPqrs(){
    if (table_prs != null) {
        table_prs.clear();
        table_prs.destroy();
        table_prs = null;
    }
    table_prs = $('#table-pqrs').DataTable({
                "scrollX": true,
                "ajax":{
                    url: 'controller/PqrsController.php',
                    type: "GET",
                    headers: {'Authorization':localStorage['token']},
                },
                "columns": [
                    {title: 'Id', data: 'id'},
                    {title: 'Usuario', data: 'user_name'},
                    {title: 'Descripción', data: 'description'},
                    {title: 'Tipo', orderable: false,"render": function ( data, type, full, meta ) {
                        if (full.type == 'P') {
                            return 'Petición';
                        }else if(full.type == 'Q'){
                            return 'Queja';
                        }else if(full.type == 'R'){
                            return 'Reclamo';
                        }else{
                            return 'Desconocido';
                        }
                    }},
                    {title: 'Estado', orderable: false,"render": function ( data, type, full, meta ) {
                        if (full.status == 'NEW') {
                            return 'Nuevo';
                        }else if(full.status == 'IN_ACTION'){
                            return 'En ejecución,';
                        }else if(full.status == 'CLOSED'){
                            return 'Cerrado';
                        }else{
                            return 'Desconocido';
                        }
                    }},
                    {title: 'Fecha de creacion', data: 'creation_date'},
                    {title: 'Fecha de vencimiento', data: 'date_limit'},
                    {title: 'Notas',  orderable: false,"render": function ( data, type, full, meta ) {
                        var d = new Date();
                        var strDate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        if (strDate>full.date_limit) {
                            return 'caduco';
                        } else {
                            return 'valido';
                        }
                    }},
                    {title: 'Opciones',orderable: false,"render": function ( data, type, full, meta ) {
                        if (localStorage['rol_name']== 'GENERAL_ADMINISTRATOR' && full.status != 'CLOSED') {
                            return  '<div>'+
                                        '<a href="#" class="btn btn-success btn-circle btn-sm btn-edit" style="margin-right: 7px;">'+
                                            '<i class="fas fa-pen"></i>'+
                                        '</a>'+
                                        '<a href="#" class="btn btn-danger btn-circle btn-sm btn-delete">'+
                                            '<i class="fas fa-trash"></i>'+
                                        '</a>'
                                    '</div>';
                        } else if(localStorage['rol_name']== 'GENERAL_USER') {
                            return 'No disponible';
                        }
                    }}  
                ]    

            });

            $('#table-pqrs tbody').on( 'click', 'tr .btn-delete', function () {
                if ( $(this).parent().parent().parent().hasClass('sa-delete') ) {
                    $(this).parent().parent().parent().removeClass('sa-delete');
                }
                else {
                    table_prs.$('tr.sa-delete').removeClass('sa-delete');
                    $(this).parent().parent().parent().addClass('sa-delete');
                }
                data = table_prs.rows('.sa-delete').data();

                var r = confirm("¿Esta seguro que desea eliminar este elemento?");
                if (r == true) {
                    $.ajax({
                        url : 'controller/PqrsController.php?pqrs_id='+data[0].id,
                        type : 'DELETE',
                        headers: {'Authorization':localStorage['token']},
                        success : function(json) {
                            json = JSON.parse(json);
                            console.log(json.status);
                            if (json.status == 'success') {
                                getPqrs();
                            }else{
                                alert('No fue posible eliminarlo');
                            }
                        },
                        error : function(xhr, status) {
                            $("#alert-register-user").show();
                            $('#alert-register-user').text('error desconocido');
                        },
                
                    });
                } else {
                  
                }
            } );

            $('#table-pqrs tbody').on( 'click', 'tr .btn-edit', function (){
                if ( $(this).parent().parent().parent().hasClass('sa-edit') ) {
                    $(this).parent().parent().parent().removeClass('sa-edit');
                }
                else {
                    table_prs.$('tr.sa-edit').removeClass('sa-edit');
                    $(this).parent().parent().parent().addClass('sa-edit');
                }
                data = table_prs.rows('.sa-edit').data();
                console.log(data);
                $('#type option[value='+data[0].type+']').attr('selected','selected');
                $("textarea#description").val(data[0].description);
                status = data[0].status;
                if (status == 'NEW') {
                    $("#status").val('IN_ACTION'); 
                    $("#status").attr('value','IN_ACTION');   
                }
                if (status == 'IN_ACTION') {
                    $("#status").val('CLOSED');
                    $("#status").attr('value','CLOSED'); 
                }
                $("#id").val(data[0].id);
            });
}