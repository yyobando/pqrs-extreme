<div class="container">
        <!-- Outer Row -->
        <div class="row justify-content-center">
            <div class="col-xl-10 col-lg-12 col-md-9">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Iniciar sesion</h1>
                                    </div>
                                    <div class="alert alert-danger" role="alert" id="alert-login" style="display:none;">
                                        This is a danger alert—check it out!
                                    </div>
                                    <form class="user" id="formLogin">
                                        <div class="form-group">
                                            <input type="email" class="form-control form-control-user"
                                                id="exampleInputEmail" name="user_name" aria-describedby="emailHelp"
                                                placeholder="Enter Email Address...">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" class="form-control form-control-user"
                                                id="exampleInputPassword" placeholder="Password">
                                              <input type="hidden" name="action" value="login">  
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="customCheck">
                                                <label class="custom-control-label" for="customCheck">Remember
                                                    Me</label>
                                            </div>
                                        </div>
                                        <button  type="submit" class="btn btn-primary btn-user btn-block">
                                            Entrar
                                        </button >
                                        <hr>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
   jQuery('form#formLogin').submit(function (e) {
        $("#alert-login").hide();
      e.preventDefault();
      var jsonData = $(this).serializeArray().reduce(function(a, z) { a[z.name] = z.value; return a; }, {});
      $.ajax({
            url : 'controller/UserController.php',
            data : JSON.stringify(jsonData),
            type : 'POST',
            dataType : 'json',
            success : function(json) {
                if(json.status == "success"){
                    localStorage['token'] = json.data.token;
                    localStorage['rol_name'] = json.data.rol_name;
                    localStorage['user_id'] = json.data.user_id;
                    window.location="main.html";
                }else{
                    $("#alert-login").show();
                    $('#alert-login').text(json.message);
                }
                
            },
            error : function(xhr, status) {
                $("#alert-login").show();
                $('#alert-login').text('error desconocido');
            },

        });
   });
</script>