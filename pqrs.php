<?php 
?> 
    <div class="row">
        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Pqrs</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="table-pqrs" width="100%" cellspacing="0">
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Nuevo Pqrs</h6>
                    
                </div>
                <div class="card-body">
                    <div class="alert alert-info" role="alert" id="alert-qprs" style="display: none;">
                        This is a danger alert—check it out!
                    </div>
                    <form class="user" id="form-pqrs">
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <select class="form-control" name="type" required id="type">
                                    <option value="">Tipo de pqr</option>
                                    <option value="P">Petición</option>
                                    <option value="Q">Queja</option>
                                    <option value="R">Reclamo</option>
                                </select>
                                
                            </div>
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" value="NEW"  name="status" class="form-control" id="status">
                                <input type="hidden" value=""  name="id" id="id">
                               
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 mb-3 mb-sm-0">
                                <textarea name="description" id="description"  class="form-control" placeholder="Asunto" required id="description"></textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-user btn-block">
                                Enviar
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
          $('form#form-pqrs').submit(function (e) {
            $('#alert-qprs').hide();
            e.preventDefault();
            var jsonData = $(this).serializeArray().reduce(function(a, z) { a[z.name] = z.value; return a; }, {});
            jsonData.user_id = localStorage['user_id'];
            $.ajax({
                    url : 'controller/PqrsController.php',
                    data : JSON.stringify(jsonData),
                    type : 'POST',
                    headers: {'Authorization':localStorage['token']},
                    dataType : 'json',
                    success : function(json) {
                        if(json.status == "success"){
                            $("#id").val('');
                            getPqrs();
                        }else{
                            $("#alert-qprs").show();
                            $('#alert-qprs').text(json.message);
                        }
                        
                    },
                    error : function(xhr, status) {
                        $("#alert-register-user").show();
                        $('#alert-register-user').text('error desconocido');
                    },

            });
    });

    </script>
   <?php
