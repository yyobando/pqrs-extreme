<?php 



class User {
    var $id;
    var $userName;
    var $password;
    var $role;
    
    function __construct($field, $value) {
      
    }
    
    public function getUser($filter)
    {
        $connection = new Connection();
        $where = (!empty($filter)) ? "WHERE $filter": '';

        $query_user = "SELECT T1.id, T1.user_name, T1.password, T2.name AS rol_name, T3.name AS status_name FROM user AS T1 
            INNER JOIN roles AS T2 ON T1.role = T2.id
            INNER JOIN user_status AS T3 ON T1.status = T3.id
            LEFT  JOIN user_token AS T4 ON T1.id = T4.user_id 
             $where;";
           
            return  $connection->execute($query_user);
    }

    public function registerUser($data)
    {
        if (!empty($data->user_name) && !empty($data->password) && !empty($data->status) && !empty( $data->role)) {
            if (!filter_var($data->user_name, FILTER_VALIDATE_EMAIL)) {
                return  array('error' => "el correo $data->user_name no es valido");
            }
            $connection = new Connection();
            $response = array();
            $user = $this->getUser("T1.user_name='$data->user_name'");
            if (!$user) {
            $data_bd = array('user_name' => $data->user_name, 'password' => md5($data->password), 'status' => $data->status, 'role' => $data->role);
            $result = $connection->save($data_bd, 'user');
            if ($result) {
                    $response = array('success' => "El Usuario $data->user_name fue registrado", 'error' => null);
            }else{
                    $response = array('error' => "El Usuario $data->user_name No pudo ser registrado");
            }
            }else{
                $response = array('error' => "El Usuario $data->user_name ya existe");
            }
        }else{
            $response = array('error' => "Todos los campos son requeridos");
        } 
        
        return $response;
    }

     public function userLogin($data)
    {   
        $response = array();
        $token = new Token();
        if (isset($data->user_name) && isset($data->password)) {
            $user = $data->user_name;
            $password = md5($data->password);

            $result = $this->getUser( "T1.user_name = '$user'" );
        
            if ($result) {
                $result = $result[0];
                if ($password == $result['password']) {
                    $user_id = $result['id'];
                    $result_token = $token->getTokenByUserId($user_id);
                    
                    if ($result_token) {
                        $result_update = $token->updateToken($result_token[0]['id']);
                    }else{

                        $result_update = $token->saveToke($user_id);
                    }

                    if ($result_update) {

                        $result_token = $token->getTokenByUserId($user_id);
                        
                        $response = array(
                            'error' => null,
                            'user_id' => $user_id,
                            'user_name' => $user,
                            'rol_name' => $result['rol_name'],
                            'status_name' => $result['status_name'],
                            'token' => $result_token[0]['token']

                        );


                    }else{
                        $response = array('error' => 'No se puedo registrar el token');
                    }
                }else{
                    $response = array('error' => "Contraseña incorrecta"); 
                }
            }else {
            $response = array('error' => "El usuario $user no existe");  
            }

        }else{
            $response = array('error' => "Campor requeridos"); 
        }
    
        return $response;
    }

    
}