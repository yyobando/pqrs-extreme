<?php 

class Token {
    
    function __construct() {
      
    }

    public function saveToke($user_id)
    {
        $val = true;
        $token = bin2hex(openssl_random_pseudo_bytes(16,$val));
        $date_add = date("Y-m-d H:i");
        $expires = date("Y-m-d H:i",strtotime($date_add."+ 1 week")); 
        $connecction = new Connection();

        $new_token = array('user_id' => $user_id, 'token' => $token,  'expires_token' => $expires, 'added_on' => $date_add);

        return $result = $connecction->save($new_token, 'user_token') ;
    }


    public function updateToken($id_token)
    {
        $val = true;
        $token = bin2hex(openssl_random_pseudo_bytes(16,$val));
        $date_add = date("Y-m-d H:i");
        $expires = date("Y-m-d H:i",strtotime($date_add."+ 1 week")); 
        $connecction = new Connection();

        $token = array('token' => $token, 'expires_token' => $expires, 'added_on' => $date_add);

        return $connecction->update($token, 'user_token', "id = $id_token");
    }

    public function getTokenByUserId($user_id)
    {
        $connecction = new Connection();
        $query = "SELECT * FROM user_token AS T1  WHERE T1.user_id = '$user_id';";
        return $connecction->execute($query);
    }

    

    public function validateToken($token)
    {
        if (!empty($token)) {
            $query = "SELECT * FROM user_token AS T1  WHERE T1.token = '$token';";
            error_log($query);
            $connecction = new Connection();
            $result = $connecction->execute($query);
            
            if ($result) {
                $date =  strtotime(date("Y-m-d H:i",time()));
                $expire_date = $result[0]['expires_token'];
                if ($expire_date > $date) {
                    $response['error'] = 'El token esta vencido';
                    header("HTTP/1.1  401 Unauthorized");
                    echo json_encode($response);
                    exit;
                }

            }else{
                $response['error'] = 'No Tiene autorizacion';
                header("HTTP/1.1  401 Unauthorized");
                echo json_encode($response);
                exit;  
            }
        }else{
            $response['error'] = 'No Tiene autorizacion';
            header("HTTP/1.1  401 Unauthorized");
            echo json_encode($response);
            exit;
        }
        
        
    }
}