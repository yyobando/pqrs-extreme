<?php
class Pqrs {
    
    function __construct() {     
    }

    public function savePqrs($data)
    { 
        $connection = new Connection();
        $user = new User(null,null);
        $user_r = $user->getUser("T1.id= '$data->user_id'");
        if ($user_r) {
            $date_add = date("Y-m-d H:i");
            $days = 0;
            if ($data->type == 'P') {
                $days = 7;
            }elseif ($data->type == 'Q') {
                $days = 3;
            }elseif ($data->type == 'R') {
                $days = 2;
            }else {
                return array('error' => 'debe definir un estado');
            }
            $expires = date("Y-m-d H:i",strtotime($date_add."+ $days days")); 
            $data_bd = array('description' => $data->description, 'status' => 'NEW', 'type' => $data->type, 'user_id' => $data->user_id, 'creation_date' => $date_add, 'date_limit' => $expires);
            $result_add = $connection->save($data_bd,'pqrs');
            if ($result_add) {
                return array('error' => null ,'success' => 'pqrs registrada con exito');
            } else {
                return array('error' => 'error al registrar');
            }
            
            
        }else{
            return array('error' => 'El id de usuario no existe');
        }
    }

    public function getPqrs($filter)
    {
        $connection = new Connection();
        $where = (!empty($filter)) ? "WHERE $filter": '';
        $query = "SELECT T2.id, T1.id AS user_id, T1.user_name,  T2.description , T2.status, T2.type, T2.`creation_date`, T2.`date_limit`  FROM user AS T1 
        INNER JOIN pqrs AS T2 ON T1.id = T2.user_id  $where";

        return  $connection->execute($query);
    }

    public function getPqrsByUser($token)
    {
        $user = new User(null,null);
        
        $user_result = $user->getUser("T4.token = '$token'");
        if ($user_result) {
           if ($user_result[0]['rol_name'] == 'GENERAL_ADMINISTRATOR') {

            $pqrs = $this->getPqrs(null);

           }elseif ($user_result[0]['rol_name'] == 'GENERAL_USER') {

                $user_id = $user_result[0]['id'];
                $pqrs = $this->getPqrs("T1.id = '$user_id'");

           }else {
            return array('error' => 'perfil no definido');
           }
    
           if ($pqrs) {
               return array('error' => null, 'data' => $pqrs);
           }else{
                return array('error' => 'No se encontraron datos');
           }
        }else{
            return array('error' => 'no existe el usuario');  
        } 

    }

    public function editPqrs($data)
    {
        $status_new = $data->status;
        $pqrs_old = $this->getPqrs("T2.id = $data->id"); 
        $connection = new Connection();
        if ($pqrs_old) {
            $pqrs_old = $pqrs_old[0];
            $status_old = $pqrs_old['status'];
            if ($status_old == 'NEW' && !$status_new == 'IN_ACTION') {
                return array('error' => 'esta pqrs solo se puede actualizar solo al estado:  EN EJECUCIÓN');
            }
            if ($status_old == 'IN_ACTION' && !$status_new == 'CLOSED') {
                return array('error' => 'esta pqrs solo se puede actualizar solo al estado:  CERRADO');
            }

            if ($status_old == 'CLOSED') {
                return array('error' => 'pqrs cerrada ya no se puede editar');
            }

            $data_bd = array('description' => $data->description, 'status' => $status_new);
             
            $result_edit = $connection->update($data_bd, 'pqrs', " id='$data->id'");
            if ($result_edit) {
                return array('error' => null, 'success' => 'pqrs actualizada');
            }else{
                return array('error' => 'ocurrio un error al actualizar'); 
            }
        }else{
            return array('error' => 'no se encontro la pqrs'); 
        }
    }

    public function deletePqrs($pqrs_id)
    {
        $connection = new Connection();
        return $connection->delete('pqrs',$pqrs_id);
    }
}    